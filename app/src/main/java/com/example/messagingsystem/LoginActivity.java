package com.example.messagingsystem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity {

    private Button loginActivity_btn_goJoin, loginActivity_btn_login;
    private EditText loginActivity_et_id,loginActivity_et_pw;
    private Intent intent;
    private String restUrl = "http://192.168.253.24:8085/MessagingSystem/rest.ws";
    private CheckBox chkAuto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // 자동로그인
        SharedPreferences shared = getSharedPreferences("auto_login", MODE_PRIVATE);
        String userId = shared.getString("userId", null);
        String userPw = shared.getString("userPw", null);
        String auto = shared.getString("auto", "no");

        if("ok".equals(auto)) {
            login(userId, userPw);
        }


        loginActivity_btn_goJoin = findViewById(R.id.loginActivity_btn_goJoin);
        loginActivity_btn_login = findViewById(R.id.loginActivity_btn_login);
        loginActivity_et_id = findViewById(R.id.loginActivity_et_id);
        loginActivity_et_pw = findViewById(R.id.loginActivity_et_pw);
        chkAuto = findViewById(R.id.loginActivity_checkBox_auto);

        loginActivity_btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userID = loginActivity_et_id.getText().toString();
                String userPassword = loginActivity_et_pw.getText().toString();

                if (userID.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Please insert Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (userPassword.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Please insert Password", Toast.LENGTH_SHORT).show();
                    return;
                }

                login(userID, userPassword);
            }
        });




        loginActivity_btn_goJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(LoginActivity.this, JoinActivity.class);
                startActivity(intent);
            }
        });



    }
    private void login(String id, String pw) {
        try {

            RequestBody requestBody =
                    new FormBody.Builder()
                            .add("cmd","login")
                            .add("userID",id)
                            .add("userPassword",pw)
                            .build();

            Request request = new Request.Builder().url(restUrl).post(requestBody).build();

            OkHttpClient client = new OkHttpClient();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("JS", "Network Error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, "인터넷 연결이 원활하지않습니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, okhttp3.Response response) throws IOException {
                    String res = response.body().string();

                    try {
                        JSONObject jObj = new JSONObject(res);

                        //로그인 성공
                        if("true".equals(jObj.getString("result"))) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    SharedPreferences sharedAuto = getSharedPreferences("auto_login", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editAuto = sharedAuto.edit();
                                    editAuto.clear();
                                    editAuto.commit();


                                    Toast.makeText(LoginActivity.this, "로그인에 성공하였습니다.", Toast.LENGTH_SHORT).show();

                                    if (chkAuto.isChecked()){
                                        SharedPreferences shared = getSharedPreferences("auto_login", MODE_PRIVATE);
                                        SharedPreferences.Editor edit = shared.edit();
                                        edit.putString("userId", loginActivity_et_id.getText().toString());
                                        edit.putString("userPw", loginActivity_et_pw.getText().toString());
                                        edit.putString("auto", "ok");
                                        edit.commit();
                                    } else {
                                        SharedPreferences shared = getSharedPreferences("auto_login", MODE_PRIVATE);
                                        SharedPreferences.Editor edit = shared.edit();
                                        edit.putString("userId", loginActivity_et_id.getText().toString());
                                        edit.putString("userPw", loginActivity_et_pw.getText().toString());
                                        edit.putString("auto", "no");
                                        edit.commit();
                                    }
                                    intent = new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        }
                        //로그인실패
                        else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                    builder.setTitle("로그인 실패").setMessage("입력하신 정보를 확인해주세요.").setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    builder.create().show();
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}