package com.example.messagingsystem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class JoinActivity extends AppCompatActivity {
    
    private Button joinActivity_btn_join, joinActivity_btn_goLogin,joinActivity_btn_idCheck;
    private EditText joinActivity_et_id,joinActivity_et_pw,joinActivity_et_pw_check,joinActivity_et_companyCode;
    private String restUrl = "http://192.168.253.24:8085/MessagingSystem/rest.ws";

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        
        joinActivity_et_id =findViewById(R.id.joinActivity_et_id);
        joinActivity_et_pw =findViewById(R.id.joinActivity_et_pw);
        joinActivity_et_pw_check =findViewById(R.id.joinActivity_et_pw_check);
        joinActivity_et_companyCode =findViewById(R.id.joinActivity_et_companyCode);

        joinActivity_btn_idCheck =findViewById(R.id.joinActivity_btn_idCheck);
        joinActivity_btn_join =findViewById(R.id.joinActivity_btn_join);
        joinActivity_btn_goLogin =findViewById(R.id.joiniActivity_btn_goLogin);

        joinActivity_btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userID = joinActivity_et_id.getText().toString();
                String userPassword = joinActivity_et_pw.getText().toString();
                String userPwCheck = joinActivity_et_pw_check.getText().toString();
                String userCompanyCode = joinActivity_et_companyCode.getText().toString();

                if (userID.isEmpty()){
                    Toast.makeText(JoinActivity.this, "Please insert Email", Toast.LENGTH_SHORT).show();
                    joinActivity_et_id.requestFocus();
                    return;
                }
                if (userPassword.isEmpty()){
                    Toast.makeText(JoinActivity.this, "Please insert Password", Toast.LENGTH_SHORT).show();
                    joinActivity_et_pw.requestFocus();
                    return;
                }
                if (userPwCheck.isEmpty()){
                    Toast.makeText(JoinActivity.this, "Please insert PasswordCheck", Toast.LENGTH_SHORT).show();
                    joinActivity_et_pw_check.requestFocus();
                    return;
                }
                if (userCompanyCode.isEmpty()){
                    Toast.makeText(JoinActivity.this, "Please insert ComapnayCode", Toast.LENGTH_SHORT).show();
                    joinActivity_et_companyCode.requestFocus();
                    return;
                }

                /*비밀번호 유효성검사*/
                if (!userPassword.equals(userPwCheck)){
                    Toast.makeText(JoinActivity.this, "비밀번호와 비밀번호 확인이 일치하지 않습니다.", Toast.LENGTH_SHORT).show();
                    joinActivity_et_pw.setText("");
                    joinActivity_et_pw_check.setText("");
                    joinActivity_et_pw_check.requestFocus();
                    return;
                }
                /*비밀번호 유효성검사*/

                try {

                    RequestBody requestBody =
                            new FormBody.Builder()
                                    .add("cmd","join")
                                    .add("userID",userID)
                                    .add("userPassword",userPassword)
                                    .add("userCompanyCode",userCompanyCode)
                                    .build();

                    Request request = new Request.Builder().url(restUrl).post(requestBody).build();

                    OkHttpClient client = new OkHttpClient();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            Log.d("JS", "Network Error");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(JoinActivity.this, "인터넷 연결이 원활하지않습니다.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Call call, okhttp3.Response response) throws IOException {
                            String res = response.body().string();

                            try {
                                JSONObject jObj = new JSONObject(res);

                                if("true".equals(jObj.getString("result"))) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(JoinActivity.this, "회원가입에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                                            intent = new Intent(JoinActivity.this,LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        joinActivity_btn_goLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(JoinActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}